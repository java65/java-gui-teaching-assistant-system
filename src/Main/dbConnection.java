package Main;
import java.sql.*;
import javax.swing.JOptionPane;
public class dbConnection {
    
    public static Connection Conn(){
      try{
        String host ="jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String pass = "1234";  
        // Clas.forname คือ การโหลดไดเวอร์ ในนี้โหลด postgresql
        Class.forName("org.postgresql.Driver");
        // Connection เป็นการสร้างการเชื่อมต่อ
        Connection conn = DriverManager.getConnection(host,user,pass);
        //เราใช้เมธอด   getConnection(“URL ของฐานข้อมูล”); 
        //หรือ getConnection(“URL ของฐานข้อมูล”, “ชื่อผู้ใช้”, “รหัสผ่าน”); ที่อยู่ในคลาส DriverManager
        //JOptionPane.showMessageDialog(null,"Connected");
        return conn;
      }catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
        return null;  
      }
    }
    
    
}
